__author__ = 'liubei'


import datetime, calendar, time

#----------------------------------------------------------------------
def timestamp2datetime(timestamp):
    """"""
    return datetime.datetime.fromtimestamp(timestamp)

#----------------------------------------------------------------------
def date2timestamp(date):
    """local time"""
    return calendar.timegm(date.timetuple())

#----------------------------------------------------------------------
def date2timestamp_utc(date):
    """utc time"""
    return int(time.mktime(date.timetuple()))

#----------------------------------------------------------------------
def datetime2timestamp(year, month, day):
    """"""
    return calendar.timegm(datetime.datetime(year, month, day).timetuple())

#----------------------------------------------------------------------
def time2timestamp(python_time):
    """"""
    return int(python_time)