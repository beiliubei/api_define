#AZ-Server api定义

##api version
名字 | 版本 | 日期|备注
------------ | ------------- | ------------| -------------|
FrankLiu | v 0.1  | 2014/12/2 | 初始化
FrankLiu | v 0.11  | 2014/12/17 | 修改：血压添加记录一天一个；优先级：1-6，16-24
FrankLiu | v 0.12 | 2014/12/17 |添加：健康知识问答
FrankLiu | v 0.13 | 2014/12/18 |完成：查看健康知识 25-27
FrankLiu | v 0.14 | 2014/12/19 |完成：戒烟日记 31-32
FrankLiu | v 0.15 | 2014/12/19 |添加：运动日记 33-36
FrankLiu | v 0.16 | 2014/12/23 |完成：运动日记 33-36
FrankLiu | v 0.17 | 2014/12/25 |更新：健康知识问答，选择题类型
FrankLiu | v 0.18 | 2014/12/26 |更新：dashboard api 添加运动
FrankLiu | v 0.19 | 2015/01/04 |完成：提交问卷调查
FrankLiu | v 0.20 | 2015/01/06 |添加：服药管理 38-40
FrankLiu | v 0.21 | 2015/01/07 |更新：40.服药管理 添加 done字段
FrankLiu | v 0.22 | 2015/01/07 |更新：39.删除服药情况 添加 根据日期 删除
FrankLiu | v 0.23 | 2015/01/07 |更新：11.获取用户信息 添加返回字段加入 用户uid
							  |||     12.修改用户信息 去除修改手机号字段
FrankLiu | v 0.24 | 2015/01/08 |完成：15.dashboard 添加服药管理数据
FrankLiu | v 0.25 | 2015/01/09 |更新：7.获取我的看护者列表 添加看护者是否在系统中有效validate
FrankLiu | v 0.26 | 2015/01/09 |完成：8,9 添加和取消和看护者分享
FrankLiu | v 0.27 | 2015/01/12 |更新：去除添加看护者输入 昵称的参数; 去除看护者列表中name字段
FrankLiu | v 0.28 | 2015/01/12 |删除：4.验证邀请码api 删除
FrankLiu | v 0.29 | 2015/01/12 |添加：使用邀请码添加病人
FrankLiu | v 0.30 | 2015/01/16 |添加：注销api
FrankLiu | v 0.31 | 2015/01/26 |更新： 删除胆固醇，可以批量删除
FrankLiu | v 0.32 | 2015/01/27 |更新： 可以批量添加胆固醇
FrankLiu | v 0.33 | 2015/01/27 |更新： 可以批量删除血压记录
FrankLiu | v 0.34 | 2015/01/27 |更新： 可以批量添加血压
FrankLiu | v 0.35 | 2015/01/27 |更新： 可以批量添加吸烟记录
FrankLiu | v 0.36 | 2015/01/27 |更新： 可以批量添加身体健康指数
FrankLiu | v 0.37 | 2015/01/27 |更新： 可以批量删除身体健康指数
FrankLiu | v 0.38 | 2015/02/02 |更新： 获取知识专题，加入token参数，返回是否已读完全部标记
FrankLiu | v 0.39 | 2015/02/02 |更新： 获取知识专题详情，加入token参数，返回是否已读完全部标记
FrankLiu | v 0.40 | 2015/02/02 |更新： 获取健康知识主题详细内容，加入token参数
FrankLiu | v 0.41 | 2015/02/03 |更新： 登陆返回用户性别
FrankLiu | v 0.42 | 2015/02/04 |更新： 注册，修改信息时需要 isSmoke参数；登陆，查看个人信息返回 isSmoke参考
FrankLiu | v 0.43 | 2015/02/04 |更新： health index 计算，根据用户是否吸烟计算 100或者0分
FrankLiu | v 0.44 | 2015/02/05 |添加：运动目标时间 api；更新：运动返回 运动总时间和目标时间
FrankLiu | v 0.45 | 2015/02/06 |更新：问卷调查加入 下一节点和问题类型字段 请求需要加入token字段
FrankLiu | v 0.46 | 2015/02/09 |更新：用户相关api加入 patientCode字段
FrankLiu | v 0.47 | 2015/02/11 |更新：获取问卷调查，返回参数
FrankLiu | v 0.48 | 2015/02/25 |更新：个人信息加入 hasHighBp 是否高血压患者属性
FrankLiu | v 0.49 | 2015/02/26 |更新：注册时、修改个人信息时，患者编号已存在 返回409
FrankLiu | v 0.50 | 2015/02/28 |添加：获取未读的知识数量
FrankLiu | v 0.51 | 2015/03/02 |更新：运动项目列表加入强度字段
FrankLiu | v 0.52 | 2015/03/03 |添加：删除运动记录
FrankLiu | v 0.53 | 2015/03/05 |更新：胆固醇记录加入TC
FrankLiu | v 0.54 | 2015/03/18 |更新：登录和获取个人信息返回用户创建时间
FrankLiu | v 0.55 | 2015/03/31 |更新：可以批量添加服药记录
FrankLiu | v 0.56 | 2015/03/31 |更新：登录返回用户剩余药片，返回用户第一次完成问卷时间
FrankLiu | v 0.57 | 2015/04/01 |更新：提交问卷调查，支持使用问卷名称做参数
FrankLiu | v 0.58 | 2015/04/01 |更新：登录返回最后一次完成的问卷名字
FrankLiu | v 0.59 | 2015/04/01 |更新：获取个人信息，返回用户剩余药片，返回用户第一次完成问卷时间，最后一次完成的问卷名字
FrankLiu | v 0.60 | 2015/04/02 |更新：可以获取全部的知识问答题目，且题目只在时间范围内的
FrankLiu | v 0.61 | 2015/04/02 |更新：提交答案，不分类
FrankLiu | v 0.62 | 2015/04/08 |更新：修改个人信息，加入可以修改剩余药片数量；取消提交服药记录的剩余药片数量
FrankLiu | v 0.63 | 2015/04/09 |添加：设置剩余药片数量
FrankLiu | v 0.64 | 2015/04/29 |更新：获取病人列表中，返回运动时间
Zihao Liu | v 0.65 | 201/01/22 |添加：添加，删除，获取血糖记录



## <a name='toc'>Table of Contents</a>

  1. [登录](#login)
  1. [给手机号发送验证码](#validatecode)
  1. [验证验证码是否正确](#validate)
  1. [~~验证邀请码<strong>(删除)</strong>~~](#validate_invitecode)
  1. [注册](#register)
  1. [重置密码](#restpwd)
  1. [获取我的看护者列表](#caregiverlist)
  1. [和我的看护者分享](#caregivershare)
  1. [取消和我的看护者分享](#caregivershare_cancel)
  1. [添加我的看护者](#caregiveradd)
  1. [获取用户信息](#profile_show)
  1. [修改用户信息](#profile_update)
  1. [获取与我分享记录的用户列表](#sharedpatientlist)
  1. [获取问卷调查](#survey)
  1. [获取健康状态dashboard](#dashboard)
  1. [添加血压记录](#bp_add)
  1. [删除血压记录](#bp_delete)
  1. [获取血压记录](#bplist)
  1. [添加bmi](#bmi_add)
  1. [删除bmi](#bmi_delete)
  1. [获取bmi](#bmilist)
  1. [添加胆固醇记录](#cholesterol_add)
  1. [删除胆固醇记录](#cholesterol_delete)
  1. [获取胆固醇记录](#cholesterollist)
  1. [获取健康知识分类](#topiclist)
  1. [获取健康知识分类详情](#topicdetail)
  1. [获取健康知识文章详情](#articledetail)
  1. [获取健康知识主题中的题目（参加健康小问答）](#questionlist)
  1. [提交健康知识问答答案（参加健康小问答）](#questionanswer)
  1. [查看健康知识问答答案（参加健康小问答）](#record)
  1. [获取吸烟记录](#smokelist)
  1. [添加吸烟记录](#smoke_add)    
  1. [获取运动日记](#exerciseloglist)
  1. [设定运动消耗卡路里目标](#targetCalorieTotalCount)
  1. [添加运动日记](#exerciselog_add)
  1. [获取运动项目列表](#sporteventlist)
  1. [提交问卷调查](#surveyresult)
  1. [提交服药情况](#medicineManagement)
  1. [提交剩余药片](#medicineCount)
  1. [~~[删除服药情况](#deleteMedicineManagement)(删除)</strong>~~]
  1. [获取服药情况](#medicineList)
  1. [添加我的病人](#patientadd)
  1. [注销](#logout)
  1. [设定运动目标时间](#targetTotalMinute)
  1. [获取未读知识文章数量](#unreadarticlecount)
  1. [删除运动记录](#deleteExerice)
  1. [添加血糖记录](#glu_add)
  1. [删除血糖记录](#glu_delete)
  1. [获取血糖记录](#glu_list)

  

###[[⬆]](#toc) <a name='login'>1.登录</a>
	post /rest/v1/login

####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPhone | String  | 用户手机号| NO
userPwd | String  | 密码| NO

####Request Example
	{
		"userPhone":"13111111111",
		"userPwd":"oo"
	}

####Response Code
	Status: 200 OK
	Status: 401 用户账号密码错误
	Status: 404 用户不存在
	Status: 500 请求参数异常/服务器异常

####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String  | 用户id(8-4-4-4-12)| NO
userPhone | String | 用户名，用于登录| NO
nickName | String | 用户昵称，用于显示| NO
userType | int | 1:病人 2:看护者 3:独立应用用户 | NO
userHeight | int | 身高 | NO
userGender | int | 1: 男 2: 女 | NO
isSmoke | int | 1:吸烟 0:不吸烟 | NO
hasHighBp | int | 1:有高血压 0:没有高血压 | NO
patientCode | String |患者编号 | NO
createDate | long | 用户创建时间 | NO
remainingMedicine | int | 剩余药片(新用户首次使用 返回-1) | NO
firstDoneSurveyTime | long |第一次完成调查时间| NO
lastDoneSurvey | String | 最后一次完成的问卷名字 | YES

####Response Example Body
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"userPhone":"13198789878",
		"nickName":"火柴",
		"userType":1,
		"userGender":1,
		"isSmoke":1,
		"hasHighBp":1,
		"userHeight":165,
		"patientCode":"E2332123",
        "createDate":123232323,
        "remainingMedicine":19,
        "firstDoneSurveyTime":0,
        "lastDoneSurvey":"BMG-G"
	}
	
- - - -

###[[⬆]](#toc) <a name='validatecode'>2.给手机号发送验证码</a>
	post /rest/v1/validatecode
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPhone | String  | 用户手机号| NO
type | int | 1:注册使用 2:重置密码使用 | NO

####Request Example
	{
		"userPhone":"13198789878",
		"type":1
	}

####Response Code
	Status: 200 OK
	Status: 404 用户不存在
	Status: 409 用户已存在
	Status: 500 请求参数异常/服务器异常
	
- - - -

###[[⬆]](#toc) <a name='validate'>3.验证验证码是否正确</a>
	post /rest/v1/validate
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPhone | String  | 用户手机号| NO
validateCode | int  | 验证码(6位数字)| NO

####Request Example
	{
		"userPhone":"13198789878",
		"validateCode":000000
	}

####Response Code
	Status: 200 OK
	Status: 404 验证码错误
	Status: 500 请求参数异常/服务器异常
	
- - - -


###[[⬆]](#toc) <a name='validate_invitecode'>~~4.验证邀请码<strong>(删除)</strong>~~</a>
	post /rest/v1/validate_invitecode
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPhone | String  | 用户手机号| NO
inviteCode | int  | 邀请码(6位数字)| NO

####Request Example
	{
		"userPhone":"13198789878",
		"inviteCode":111111
	}

####Response Code
	Status: 200 OK
	Status: 404 验证码错误
	Status: 500 请求参数异常/服务器异常
- - - -

###[[⬆]](#toc) <a name='register'>5.注册</a>
	post /rest/v1/register
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPhone | String  | 用户手机号| NO
userPwd | String  | 密码| NO
nickName | String | 用户昵称 | NO
userBirthday | String | 生日(yyyy-MM-dd) | NO
userHeight |int | 身高 | NO
userGender | int | 1: 男 2: 女 | NO
userType | int | 1:病人 2:看护者 3:独立应用用户 | NO
isSmoke | int | 1:吸烟 0:不吸烟 | NO
hasHighBp | int | 1:有高血压 0:没有高血压 | NO
patientCode | String |患者编号 | NO

####Request Example
	{
		"userPhone":"13198789878",
		"userPwd":"oo",
		"nickName":"Frank",
		"userBirthday":"1989-03-02",
		"userHeight":185,
		"userGender":1,
		"userType":1,
		"isSmoke":1,
		"hasHighBp":1,
		"patientCode":"E1234212"
	}

####Response Code
	Status: 200 OK
	Status: 409 患者编号已存在
	Status: 500 请求参数异常/服务器异常

####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String  | 用户id(8-4-4-4-12)| NO
userPhone | String | 用户手机号，用于登录| NO
nickName | String | 用户昵称，用于显示| NO
userType | int | 1:病人 2:看护者 | NO
userGender | int | 1:男 2:女 | NO
isSmoke | int | 1:吸烟 0:不吸烟 | NO
hasHighBp | int | 1:有高血压 0:没有高血压 | NO
remainingMedicine | int | 剩余药片(新用户首次使用 返回-1) | NO
patientCode | String |患者编号 | NO
createDate | long | 用户创建时间 | NO

####Response Example Body
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"userPhone":"13198789878",
		"nickName":"Frank",
		"userType":1,
		"userGender":1,
		"isSmoke":1,
		"hasHighBp":1,
		"patientCode":"E1234212",
		"createDate":2323132,
		"remainingMedicine"":-1
	}
	
- - - -

###[[⬆]](#toc) <a name='restpwd'>6.重置密码</a>
	post /rest/v1/restpwd
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPwd | String  | 密码| NO
userPhone | String | 用户手机号，用于登录| NO

####Request Example
	{
		"userPhone":"13198789878",
		"userPwd":"123232"
	}

####Response Code
	Status: 200 OK
	Status: 404 用户不存在（通过手机验证码流程验证，理论上不存在这种情况）
	Status: 500 请求参数异常/服务器异常
- - - -

###[[⬆]](#toc) <a name='caregiverlist'>7.获取我的看护者列表</a>
	get /rest/v1/caregiverlist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO

####Request Example
	?accessToken=12345643jlkhiaki

####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常

####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | 用户id(8-4-4-4-12)| NO
nickName | String | 昵称 |NO
createDate | long | 添加时间(11/13位) | NO
shared | int | 1:已分享 0:未分享 | NO
userPhone | String | 看护者手机号 | NO
validate | int | 1:已验证(在系统中存在，且用户类型是看护者) 0:未验证(系统中不存在，或者看护者类型不是看护者) | NO

####Response Example Body
	[{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"createDate":1417417881,
		"nickName":"Frank",
		"shared":1,
		"validate":1,
		"userPhone":18001822238
	},
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdeg",
		"createDate":1417417881,
		"nickName":"David",
		"shared":0,
		"validate":0,
		"userPhone":18001822231
	}]
	
- - - -

###[[⬆]](#toc) <a name='caregivershare'>8.和我的看护者分享</a>
	post /rest/v1/caregivershare
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String  | 看护者id(8-4-4-4-12)| NO


####Request Example
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef"
	}

####Response Code
	Status: 200 OK
	Status: 404 看护者不存在
	Status: 500 请求参数异常/服务器异常
	
- - - -

###[[⬆]](#toc) <a name='caregivershare_cancel'>9.取消和我的看护者分享</a>
	delete /rest/v1/caregivershare
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String  | 看护者id(8-4-4-4-12)| NO


####Request Example
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef"
	}

####Response Code
	Status: 200 OK
	Status: 404 看护者不存在
	Status: 500 请求参数异常/服务器异常
	
- - - -

###[[⬆]](#toc) <a name='caregiveradd'>10.添加我的看护者</a>
	post /rest/v1/caregiveradd
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
userPhone | String | 看护者手机号码 | NO


####Request Example
	{
		"accessToken":"xxxxf313213",
		"userPhone":"13198987867"
	}

####Response Code
	Status: 200 OK
	Status: 409 手机号已被注册成病人
	Status: 500 请求参数异常/服务器异常
	
	
####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"createDate":1417417881,
		"nickName":"Frank",
		"shared":0,
		"validate":0,
		"userPhone":18001822238
	}
- - - -

###[[⬆]](#toc) <a name='profile_show'>11.获取我的用户信息</a>
	get /rest/v1/profile/show
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String | 用户token | NO
uid | String | 用户Id(查看自己可以不传，未来用来查看他人主页) | YES

####Request Example
	?accessToken=12345643jlkhiaki
	
####Response Code
	Status: 200 OK
	Status: 404 用户不存在
	Status: 401 用户token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPhone | String  | 用户手机号| NO
nickName | String | 用户昵称 | NO
userBirthday | String | 生日(yyyy-MM-dd) | NO
userHeight |int | 身高 | NO
userGender | int | 1: 男 2: 女 | NO
userType | int |1: 病人 2: 看护者 | NO
uid | String | 用户id | NO
isSmoke | int | 1:吸烟 0:不吸烟|NO
hasHighBp | int | 1:有高血压 0:没有高血压 | NO
patientCode | String |患者编号 | NO
createDate | long | 用户创建时间 | NO
remainingMedicine | int | 剩余药片(新用户首次使用 返回-1) | NO
firstDoneSurveyTime | long |第一次完成调查时间| NO
lastDoneSurvey | String | 最后一次完成的问卷名字 | YES

####Response Example Body
	{
		"uid":"jfdf",
		"userPhone":"13198789878",
		"nickName":"Frank",
		"userBirthday":"1989-03-02",
		"userHeight":185,
		"userGender":1,
		"userType":1,
		"isSmoke":1,
		"hasHighBp":1
		"patientCode":"E1234212",
		"createDate":23232323,
		"remainingMedicine":19,
        "firstDoneSurveyTime":0,
        "lastDoneSurvey":"BMG-G"
	}
	
- - - -

###[[⬆]](#toc) <a name='profile_update'>12.修改用户信息</a>
	post /rest/v1/profile/update
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String | 用户token | NO
nickName | String | 用户昵称 | NO
userBirthday | String | 生日(yyyy-MM-dd) | NO
userHeight |int | 身高 | NO
userGender | int | 1: 男 2: 女 | NO
isSmoke|int |1:吸烟 0:不吸烟 |NO
hasHighBp | int | 1:有高血压 0:没有高血压 | NO
patientCode | String |患者编号 | NO
remainingMedicine | int | 剩余药片(新用户首次使用 返回-1) | NO

####Request Example
	{
		"accessToken":"23131231231231",
		"nickName":"Frank",
		"userBirthday":"1989-03-02",
		"userHeight":185,
		"userGender":1,
		"isSmoke":1,
		"hasHighBp":1,
		"patientCode":"E1234561",
		"remainingMedicine":10
	}
	
####Response Code
	Status: 200 OK
	Status: 401 用户token过期
	Status: 409 患者编号已存在
	Status: 500 请求参数异常/服务器异常

####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
userPhone | String  | 用户手机号| NO
nickName | String | 用户昵称 | NO
userBirthday | String | 生日(yyyy-MM-dd) | NO
userHeight |int | 身高 | NO
userGender | int | 1: 男 2: 女 | NO
userType | int |1: 病人 2: 看护者 | NO
uid | String | 用户id | NO
isSmoke|int|1:吸烟 0:不吸烟 |NO
hasHighBp | int | 1:有高血压 0:没有高血压 | NO
patientCode | String |患者编号 | NO
createDate | long | 用户创建时间 | NO
remainingMedicine | int | 剩余药片(新用户首次使用 返回-1) | NO


####Response Example Body
	{
		"userPhone":"13198789878",
		"nickName":"Frank",
		"userBirthday":"1989-03-02",
		"userHeight":185,
		"userGender":1,
		"userType":1,
		"uid":"123",
		"isSmoke":1,
		"hasHighBp":1,
		"patientCode":"E12345671",
		"createDate":2323231,
		"remainingMedicine":10
	}
	
- - - -

###[[⬆]](#toc) <a name='sharedpatientlist'>13.获取与我分享记录的用户列表</a>

	get /rest/v1/profile/sharedpatientlist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO

####Request Example
	?accessToken=12345643jlkhiaki

####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常

####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | 用户id(8-4-4-4-12)| NO
nickName | String | 昵称 |NO
bmiIndex | double | 身体质量指数 | NO
takingDays | int | 服药天数 | NO
consumeHeatCount | double |消耗热量| YES
totalMin | int | 运动时间 | NO

####Response Example Body
	[{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"createDate":1417417881,
		"bmiIndex":198,
		"takingDays":11,
		"consumeHeatCount":2009,
		"nickName":"ff",
		"totalMin":2009
	},
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdeg",
		"createDate":1417417881,
		"bmiIndex":198,
		"takingDays":11,
		"consumeHeatCount":2009,
		"nickName":"ff",
		"totalMin":2009
	}]
	
- - - -

###[[⬆]](#toc) <a name='survey'>14.获取问卷调查</a>
	get /rest/v1/survey
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO

####Request Example
	?accessToken=12345643jlkhiaki

####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常

####Response Parameters
##### survey
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | 问卷id(8-4-4-4-12)| NO
name | String | 问卷名字 |NO
des | String | 问卷描述信息 | YES
surveies | array | 问卷集合 为空时表示当前没有问卷 | NO
firstNode | String | 问卷第一题 | NO
lastDone | long | 上一次完成问卷时间 | NO
lastSurvey | String | 上一次完成的问卷名字 | NO

##### surveies
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | 题目id(8-4-4-4-12)| NO
content | String | 题目内容 |NO
choices | array | 问卷集合 | NO
nextIds | array | 下一题集合，如果是多个结果和choices顺序对齐，如果是单个，就是一个结果 | NO
type | int | 1:日历 0: 单选 2:指定范围单选 3:未知 | YES

####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdeg",
		"name":"xxxx",
		"des":"这是一份来自xxx的调查，我们希望",
		"firstNode":"01234567-89ab-cdef-0234-456789abcdeg",
		"surveies":[
			{
				"uid":"01234567-89ab-cdef-0234-456789abcdeg",
				"content":"检测定血脂时，常在饭后12-14小时采血，这样才能较为可靠的反映血脂水平的真实情况。",
				"choices":["经常","不经常","从不"],
				"nextIds":["01234567-89ab-cdef-0234-456789abcdef"],
				"type":0
			},{
				"uid":"01234567-89ab-cdef-0234-456789abcdef",
				"content":"检测定血脂时，常在饭后12-14小时采血，这样才能较为可靠的反映血脂水平的真实情况。",
				"choices":["经常","从不"],
				"nextIds":["01234567-89ab-cdef-0234-456789abcdef","01234567-89ab-cdef-0234-456789abcdef"]
			},{
				"uid":"01234567-89ab-cdef-0234-456789abcdeh",
				"content":"检测定血脂时，常在饭后12-14小时采血，这样才能较为可靠的反映血脂水平的真实情况。",
				"choices":["经常","不经常","从不"]
			}
		]
	}
	
- - - -

###[[⬆]](#toc) <a name='dashboard'>15.获取健康状况dashboard</a>
	get /rest/v1/health/dashboard
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
profileId | String | 用户id | YES

####Request Example
	?accessToken=12345643jlkhiaki

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
##### health
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
healthIndex | int  | 健康指数(0~100)如何计算？| NO
bmis | array | 身体质量指数 (按时间排序,返回最新的10条记录) | NO
bps | array | 血压 (按时间排序,返回最新的10条记录)| NO
cholesterols | array | 胆固醇水平 (按时间排序,返回最新的10条记录) | NO
medicines | array | 服药情况 (按时间排序,返回最新的10条记录) | NO
exericelogs | array | 运动 (按时间排序,返回最新的10条记录) | NO


##### bmi  
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
weight | double | 体重(kg) | NO
height | double | 身高(cm) | NO
targetWeight | double | 目标体重(kg) | NO
bmiIndex | double | 身体质量指数 | NO
targetBmiIndex | double | 目标身体质量指数 | NO
createDate | long | 添加时间(11/13位) | NO

##### bp
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
bpHighIndex | long | 舒张压 | NO
bpLowIndex | long | 收缩压 | NO
createDate | long | 添加时间(11/13位) | NO

##### cholesterol
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
low | double | 低密度脂蛋白胆固醇(mmol/L) | NO
high | double | 高密度脂蛋白胆固醇(mmol/L) | NO
tg | double | 甘油三酯(mmol/L) | NO
tc | double | 总胆固醇|NO
createDate | long | 添加时间(11/13位) | NO

##### exericelog
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieTotalCount | int | 实际消耗卡路里 | NO
targetCalorieTotalCount|int | 目标消耗卡路里 | NO
createDate | long | 添加时间(11/13位) | NO
totalMinute | int | 运动时间 | NO
targetTotalMinute | int | 目标运动时间 | NO

##### medicine
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
createDate | long | 添加时间(11/13位) | NO
remainingMedicine | int | 剩余药片 | NO
done | int | 是否服药 | 1:服药 0:未知 | NO

####Response Example Body
	{
		"healthIndex":80,
		"bmis":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"weight":94.9,
					"bmiIndex":19.4,
					"height":170.0,
					"targetWeight":90.0,
					"targetBmiIndex":20.1,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdei",
					"weight":94.9,
					"bmiIndex":19.4,
					"height":170.0,
					"targetWeight":90.0,
					"targetBmiIndex":20.1,
					"createDate":1417449600
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdek",
					"weight":94.9,
					"bmiIndex":19.4,
					"height":170.0,
					"targetWeight":90.0,
					"targetBmiIndex":20.1,
					"createDate":1417536000
				}
			],
		"bps":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"bpHighIndex":120,
					"bpLowIndex":90,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeg",
					"bpHighIndex":120,
					"bpLowIndex":90,
					"createDate":1417363300
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdef",
					"bpHighIndex":120,
					"bpLowIndex":90,
					"createDate":1417363400
				}
			],
		"cholesterols":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"high":94.9,
					"low":19.4,
					"tg":170.0,
					"tc":1.2,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"high":94.9,
					"low":19.4,
					"tg":170.0,
					"tc":1.2,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"high":94.9,
					"low":19.4,
					"tg":170.0,
					"tc":1.4,
					"createDate":1417363200
				}
			],
		"exericelogs":[
			{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":200,
					"targetCalorieTotalCount":100,
					"createDate":1417363200
			},
			{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":200,
					"targetCalorieTotalCount":100,
					"createDate":1417363200
			},
			{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":200,
					"targetCalorieTotalCount":100,
					"createDate":1417363200
			}
		],
		"medicines":[]
	}
	
- - - -

###[[⬆]](#toc) <a name='bp_add'>16.添加血压记录</a>
	post /rest/v1/bp

####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
bpHighIndex | long | 上压 | YES
bpLowIndex | long | 下压 | YES
createDate | long | 创建日期 | YES
bps | array | 血压集合 | YES

####Request Example	
	demo1	
	{
		"accessToken":"xxxxf313213",
		"bpHighIndex":120,
		"bpLowIndex":90
	}
	demo2
	{
		"accessToken":"xxxxf313213",
		"bps":[{
			"bpHighIndex":120,
			"bpLowIndex":90,
			"createDate":2313
		}]
	}

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
bpHighIndex | long | 上压 | NO
bpLowIndex | long | 下压 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	
	demo1
	{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"bpHighIndex":120,
		"bpLowIndex":90,
		"createDate":1417363200
	}
	
		demo2
	{
		[{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"bpHighIndex":120,
		"bpLowIndex":90,
		"createDate":1417363200
	}]
		
	}
- - - -

###[[⬆]](#toc) <a name='bp_delete'>17.删除血压记录</a>
	delete /rest/v1/bp
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String | 血压记录id(为空时，删除所有记录) | YES
uids | array | 血压记录id集合 | YES

####Request Example
	demo1
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef"
	}
	demo2
	{
		"accessToken":"xxxxf313213"
	}
		demo3
	{
		"accessToken":"xxxxf313213",
		"uids":"01234567-89ab-cdef-0123-456789abcdef,01234567-89ab-cdef-0123-456789abcdef"
	}
	
	demo4
	/rest/v1/cholesterol?accessToken=xxx&uids=01234567-89ab-cdef-0123-456789abcdef,01234567-89ab-cdef-0123-456789abcdef

####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 500 请求参数异常/服务器异常
	
- - - -

###[[⬆]](#toc) <a name='bplist'>18.获取血压记录</a>
	get /rest/v1/bplist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
profileId | String | 用户id | YES
page | int | 第几页（0开始, -1获取全部数据） | YES
pageSize | int | 每页返回数量 （默认10条）| YES

####Request Example
	demo1 查看自己
	?accessToken=12345643jlkhiaki&page=0&pageSize=10
	demo2 查看他人
	?accessToken=12345643jlkhiaki&page=0&pageSize=10&profileId=123232fjfd

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
按照时间由近及远排序

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 页数| NO
bps | array | 血压集合 | NO
total | int | 血压总数 | NO

##### bp
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
bpHighIndex | long | 舒张压 | NO
bpLowIndex | long | 收缩压 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	{
		"page":0,
		"bps":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"bpHighIndex":120,
					"bpLowIndex":90,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeg",
					"bpHighIndex":120,
					"bpLowIndex":90,
					"createDate":1417363300
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdef",
					"bpHighIndex":120,
					"bpLowIndex":90,
					"createDate":1417363400
				}
			],
		"total":3
	}
	
- - - -

###[[⬆]](#toc) <a name='bmi_add'>19.添加身体质量指数记录</a>
	post /rest/v1/bmi
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
height | double | 身高(cm) | YES
weight | double | 体重(kg) | YES
targetWeight | double | 目标体重(kg) | YES
createDate | long | 创建日期 | YES

bmis | array | 集合|YES
####Request Example
	demo1
	{
		"accessToken":"xxxxf313213",
		"height":170,
		"weight":90,
		"targetWeight":88
	}
	demo2
	{
		"accessToken":"xxxxf313213",
		"bmis":[{
			"height":170,
			"weight":90,
			"targetWeight":88,
			"createDate":23123123
		}]
	}

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
weight | double | 体重(kg) | NO
height | double | 身高(cm) | NO
targetWeight | double | 目标体重(kg) | NO
bmiIndex | double | 身体质量指数(体质指数（BMI）=体重（kg）÷身高^2（m）) | NO
targetBmiIndex | double | 目标身体质量指数 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"weight":94.9,
		"bmiIndex":19.4,
		"height":170.0,
		"targetWeight":90.0,
		"targetBmiIndex":20.1,
		"createDate":1417363200
	}
	
- - - -

###[[⬆]](#toc) <a name='bmi_delete'>20.删除身体质量指数记录</a>

	delete /rest/v1/bmi
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String | 身体质量指数id(为空时，删除所有记录) | YES

####Request Example
	demo1
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef"
	}
	demo2
	{
		"accessToken":"xxxxf313213"
	}

####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 500 请求参数异常/服务器异常
	
- - - -

###[[⬆]](#toc) <a name='bmilist'>21.获取身体质量指数记录</a>
	get /rest/v1/bmilist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
profileId | String | 用户id | YES
page | int | 第几页（0开始, -1获取全部数据） | YES
pageSize | int | 每页返回数量 （默认10条）| YES

####Request Example
	?accessToken=12345643jlkhiaki&page=0&pageSize=10

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
按照时间由近及远排序

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 页数| NO
bmis | array | 集合 | NO
total | int | 总数 | NO

##### bmi
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
weight | double | 体重(kg) | NO
height | double | 身高(cm) | NO
targetWeight | double | 目标体重(kg) | NO
bmiIndex | double | 身体质量指数 | NO
targetBmiIndex | double | 目标身体质量指数 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	{
		"page":0,
		"bmis":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"weight":94.9,
					"bmiIndex":19.4,
					"height":170.0,
					"targetWeight":90.0,
					"targetBmiIndex":20.1,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdei",
					"weight":94.9,
					"bmiIndex":19.4,
					"height":170.0,
					"targetWeight":90.0,
					"targetBmiIndex":20.1,
					"createDate":1417449600
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdek",
					"weight":94.9,
					"bmiIndex":19.4,
					"height":170.0,
					"targetWeight":90.0,
					"targetBmiIndex":20.1,
					"createDate":1417536000
				}
			],
		"total":3
	}
	
- - - -

###[[⬆]](#toc) <a name='cholesterol_add'>22.添加胆固醇指数记录</a>

	post /rest/v1/cholesterol

	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
low | double | 低密度脂蛋白胆固醇(mmol/L) | YES
high | double | 高密度脂蛋白胆固醇(mmol/L) | YES
tc | double | 总胆固醇|YES
tg | double | 甘油三酯(mmol/L) | YES
create | long | 创建时间 | YES

cholesterols | array | 胆固醇集合 | YES

####Request Example
	
	demo1
	{
		"accessToken":"xxxxf313213",
		"low":1.1,
		"high":2.2,
		"tg":8.8,
		"tc":1.2
	}
	demo2
	{
		"accessToken":"xxxxf313213",
		"cholesterols":[{
			"low":1.1,
			"high":2.2,
			"tg":8.8,
			"tc":1.3,
			"createDate":23231
		},{
			"low":1.1,
			"high":2.2,
			"tg":8.8,
			"tc":1.4,
			"createDate":23232
		}]
		
	}
	

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
low | double | 低密度脂蛋白胆固醇(mmol/L) | NO
high | double | 高密度脂蛋白胆固醇(mmol/L) | NO
tg | double | 甘油三酯(mmol/L) | NO
tc | double | 总胆固醇|NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	demo1
	{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"low":1.1,
		"high":2.2,
		"tg":8.8,
		"tc":1.4,
		"createDate":1417363200
	}
	demo2
	[
 	 {
    	"uid": "54c6ef5db2c67df242f46fbd",
	    "createDate": 1422323548,
    	"high": 1,
	    "low": 1,
    	"tg": 1,
    	"tc":1.3
  	},
	  {
    	"uid": "54c6ef5db2c67df242f46fbd",
	    "createDate": 1422323548,
    	"high": 2,
	    "low": 1,
    	"tg": 2,
    	"tc":1.2
	  }
	]
	
- - - -

###[[⬆]](#toc) <a name='cholesterol_delete'>23.删除胆固醇指数记录</a>
	delete /rest/v1/cholesterol
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String | id(为空时，删除所有记录) | YES
uids | array | id集合 | YES

####Request Example
	demo1
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef"
	}
	demo2
	{
		"accessToken":"xxxxf313213"
	}
	demo3
	{
		"accessToken":"xxxxf313213",
		"uids":"01234567-89ab-cdef-0123-456789abcdef,01234567-89ab-cdef-0123-456789abcdef"
	}
	
	demo4
	/rest/v1/cholesterol?accessToken=xxx&uids=01234567-89ab-cdef-0123-456789abcdef,01234567-89ab-cdef-0123-456789abcdef

####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 500 请求参数异常/服务器异常
	
- - - -

###[[⬆]](#toc) <a name='cholesterollist'>24.获取胆固醇指数记录</a>
	get /rest/v1/cholesterollist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
profileId | String | 用户id | YES
page | int | 第几页（0开始, -1获取全部数据） | YES
pageSize | int | 每页返回数量 （默认10条）| YES

####Request Example
	?accessToken=12345643jlkhiaki&page=0&pageSize=10

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
按照时间由近及远排序

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 页数| NO
cholesterols | array | 集合 | NO
total | int | 总数 | NO

##### cholesterol
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
low | double | 低密度脂蛋白胆固醇(mmol/L) | NO
high | double | 高密度脂蛋白胆固醇(mmol/L) | NO
tg | double | 甘油三酯(mmol/L) | NO
tc | double | 总胆固醇|NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	{
		"page":0,
		"cholesterols":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"high":94.9,
					"low":19.4,
					"tg":170.0,
					"tc":1.2,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdei",
					"high":94.9,
					"low":19.4,
					"tg":170.0,
					"tc":1.3,
					"createDate":1417449600
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdek",
					"high":94.9,
					"low":19.4,
					"tg":170.0,
					"tc":1.3,
					"createDate":1417536000
				}
			],
		"total":3
	}
	
- - - -

###[[⬆]](#toc) <a name='topiclist'>25.获取健康知识分类</a>
	get /rest/v1/knowledge/topiclist

	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 第几页（0开始, -1获取全部数据） | YES
pageSize | int | 每页返回数量 （默认10条）| YES
accessToken | String | token 为空时 匿名访问 | YES

####Request Example
	?page=0&pageSize=10&accessToken=xxx

####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常

####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
topics | array | 知识主题集合 | NO
page | int | 页数 | NO
total | int | 总数 | NO

##### knowledge topic

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
title | String | 标题 | NO
des | String | 内容 | NO
image | String | 图片地址 (/image/xx.png) | NO
isRead| int | 1:已读 0:未读| NO

####Response Example Body
	{
		"topics":[
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdef",
				"title":"血脂异常分类",
				"des":"血脂异常分类相关描述信息xxxx",
				"image":"/image/xx.png",
				"isRead":1
			},
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdeg",
				"title":"血脂异常分类",
				"des":"血脂异常分类相关描述信息xxxx",
				"image":"/image/xx.png",
				"isRead":0
			}
		],
		"page":0,
		"total":2
	}
	
- - - -

###[[⬆]](#toc) <a name='topicdetail'>26.获取健康知识主题详细内容</a>

	get /rest/v1/knowledge/topic/{uid}
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | 主题id | NO
accessToken| String| token 可以匿名访问 |YES

####Request Example
	/rest/v1/knowledge/topic/01234567-89ab-cdef-0123-456789abcdeg?accessToken=xxx
	
####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
title | String | 标题 | NO
des | String | 描述内容 | NO
image | String | 图片地址 (/image/xx.png) | NO
articles | array | 文章集合 | NO

#####article
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
title | String | 标题 | NO
des | String | 描述内容 | NO
image | String | 图片地址 (/image/xx.png) | NO
isRead|int | 1:已读 0:未读 | NO

####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"title":"血脂异常分类",
		"des":"血脂异常分类相关描述信息xxxx",
		"image":"/image/xx.png",
		"articles":[
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdef",
				"title":"血脂异常分类",
				"des":"血脂异常分类相关描述信息xxxx",
				"image":"/image/xx.png",
				"isRead":1
			},
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdef",
				"title":"血脂异常分类",
				"des":"血脂异常分类相关描述信息xxxx",
				"image":"/image/xx.png",
				"isRead":0
			}
		]
	}
- - - -

###[[⬆]](#toc) <a name='articledetail'>27.获取健康知识主题详细内容</a>
	get /rest/v1/knowledge/article/{uid}
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | 文章id | NO
accessToken | String | token 可以匿名 | YES

####Request Example
	/rest/v1/knowledge/article/01234567-89ab-cdef-0123-456789abcdeg?accessToken=xxx
	
####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
title | String | 标题 | NO
des | String | 描述内容 | NO
image | String | 图片地址 (/image/xx.png) | NO
content | String | 文章详情 （html代码,注意处理特殊字符） | NO


####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"title":"血脂异常分类",
		"des":"血脂异常分类相关描述信息xxxx",
		"image":"/image/xx.png",
		"content":"<p>文章</p>"
	}
	
- - - -
###[[⬆]](#toc) <a name='questionlist'>28.获取健康知识主题中的题目（参加健康小问答）</a>
	get /rest/v1/knowledge/questionlist/{uid}
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | 主题id（如果为0，获取全部的可以参加的问答） | NO
accessToken | String  | 用户token| NO

####Request Example
	/rest/v1/knowledge/questionlist/01234567-89ab-cdef-0123-456789abcdeg?accessToken=xxxxxx
	
####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
title | String | 标题 | NO
questions | array | 题目集合 | NO

#####question
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
title | String | 题目标题 | NO
type | int | 题目类型（暂定只有一种选择题 类型为 1） | NO
choices | array | 选项 | NO

#####choice
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
des | String | 选项标题 | NO
label | String | 选项标记| NO

####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"title":"血脂异常分类",
		"questions":[
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdef",
				"title":"关于xx描述哪些是正确的？",
				"type":1,
				"choices":[{
						"label":"A",
						"des":"中国首都是北京"
					}, 
					{
						"label":"B",
						"des":"上海不是一个城市"
					},
					{
					 	"label":"C",
					 	"des":"南京是中国首都"
					}]
			},
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdef",
				"title":"关于xx描述哪些是正确的？",
				"type":1,
				"choices":[{
						"label":"A",
						"des":"中国首都是北京"
					}, 
					{
						"label":"B",
						"des":"上海不是一个城市"
					},
					{
					 	"label":"C",
					 	"des":"南京是中国首都"
					}]
			}
		]
	}
- - - -
###[[⬆]](#toc) <a name='questionanswer'>29.提交健康知识问答答案（参加健康小问答）</a>
	post /rest/v1/knowledge/questionanswer
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | 主题id(为0，是全部) | NO
accessToken | String  | 用户token| NO
answers | array | 答案集合 | NO

##### answer
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | 题目id | NO
userAnswer | String | 答案 (选择题注意顺序,如 CDA,提交时需要排序成 ACD) | NO

####Request Example
	{
		"uid":"fdlfjslf",
		"accessToken":"2313123",
		"answers":[
			{
				"uid":"fdjfd",
				"userAnswer":"ACD"
			},
			{
				"uid":"ffe",
				"userAnswer":"BD"
			}
		]
	}
	
####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
返回 最近三次的考试成绩记录，按照时间由近及远排序

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
records | array | 考试集合(<=3) | NO

##### record
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
score | int | 分数 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	{
		"records":[
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdef",
				"score":40,
				"createDate":1417363300
			},
			{
				"uid":"01234567-89ab-cdef-0123-456789abcdef",
				"score":50,
				"createDate":1417363200
			}
		]
	}
- - - -

###[[⬆]](#toc) <a name='record'>30.查看健康知识问答答案（参加健康小问答）</a>
	get /rest/v1/knowledge/record/{uid}
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | score record id | NO
accessToken | String  | 用户token| NO

####Request Example
	/rest/v1/knowledge/record/01234567-89ab-cdef-0123-456789abcdef?accessToken=xxxxxx
	
####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | record id(8-4-4-4-12)| NO
answers | array | 分数 | NO
score | int | 分数 | NO
createDate | long | 添加时间(11/13位) | NO

####answer
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
title | String | 题目标题 | NO
type | int | 题目类型（暂定只有一种对错题 类型为 1） | NO
correctAnswer | String | 题目答案 | NO
userAnswer | String | 用户提交的答案| NO
result | int | 结果 1:答对 0: 答错 | NO

####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"score":40,
		"createDate":1417363300,
		"answers":[
			{
				"uid":"ffdjf",
				"title":"这是正确的么？",
				"type":1,
				"correctAnswer":"ACD",
				"userAnswer":"ACD",
				"result":1
			},{
				"uid":"ffdj2",
				"title":"这是正确的么？",
				"type":1,
				"correctAnswer":"AD",
				"userAnswer":"A",
				"result":0
			}
		]
	}
- - - -
###[[⬆]](#toc) <a name='smokelist'>31.获取吸烟记录</a>
	get /rest/v1/smokelist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
profileId | String | 用户id | YES
page | int | 第几页（0开始, -1获取全部数据） | YES
pageSize | int | 每页返回数量 （默认10条）| YES

####Request Example
	?accessToken=12345643jlkhiaki&page=0&pageSize=10

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
按照时间由近及远排序

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 页数| NO
smokes | array | 集合 | NO
total | int | 总数 | NO

##### smoke
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
count | int | 吸烟根数 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	{
		"page":0,
		"smokes":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"count":2,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"count":2,
					"createDate":1417363200
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"count":2,
					"createDate":1417363200
				}
			],
		"total":3
	}
	
- - - -
###[[⬆]](#toc) <a name='smoke_add'>32.添加吸烟记录</a>
	post /rest/v1/smoke

	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
count | int | 吸烟根数 | YES
createDate | long | 添加时间(11/13位) | YES

smokes | array | 集合 | YES

####Request Example
	demo1
	{
		"accessToken":"2313",
		"count" : 10,
		"createDate": 1417363200
	}
	demo2
	{
		"accessToken":"2313",
		"smokes":[
			{
				"count" : 10,
				"createDate": 1417363200
			},
			{
				"count" : 10,
				"createDate": 1417363200
			}]
	}
####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
count | int | 吸烟根数 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"count":2,
		"createDate":1417363200
	}
	
- - - -
###[[⬆]](#toc) <a name='exerciseloglist'>33.获取运动日记</a>
	get /rest/v1/exerciseloglist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
profileId | String | 用户id | YES
page | int | 第几页（0开始, -1获取全部数据） | YES
pageSize | int | 每页返回数量 （默认10条）| YES

####Request Example
	?accessToken=12345643jlkhiaki&page=0&pageSize=10

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
按照时间由近及远排序

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 页数| NO
exericelogs | array | 集合 | NO
total | int | 总数 | NO

##### exericelog
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieTotalCount | int | 实际消耗卡路里 | NO
targetCalorieTotalCount|int | 目标消耗卡路里 | NO
createDate | long | 添加时间(11/13位) | NO
exerices | array | 集合 | NO
totalMinute | long | 时间总数 | NO
targetTotalMinute | long | 时间总数 | NO

##### exerice
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieCount | int | 消耗卡路里 | NO
min | int | 运动时间(分钟) | NO
type | int | 运动类型 | NO
typeName | String | 运动类型名字 | NO


####Response Example Body
	{
		"page":0,
		"exericelogs":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":200,
					"targetCalorieTotalCount":100,
					"createDate":1417363200,
					"totalMinute":38,
					"targetTotalMinute":30,
					"exerices":[
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 20,
							"min": 19,
							"type":1,
							"typeName":"跑步"
						},
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 180,
							"min": 19,
							"type":2,
							"typeName":"游泳"
						}
					]
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":30,
					"targetCalorieTotalCount":200,
					"createDate":1417363200,
					"totalMinute":38,
					"targetTotalMinute":30,
					"exerices":[
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 20,
							"min": 19,
							"type":1,
							"typeName":"跑步"
						},
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 10,
							"min": 19,
							"type":2,
							"typeName":"游泳"
						}
					]
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":300,
					"targetCalorieTotalCount":300,
					"createDate":1417363200,
					"totalMinute":38,
					"targetTotalMinute":30,
					"exerices":[
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 120,
							"min": 19,
							"type":1,
							"typeName":"跑步"
						},
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 180,
							"min": 19,
							"type":2,
							"typeName":"游泳"
						}
					]
				}
			],
		"total":3
	}
	
- - - -
###[[⬆]](#toc) <a name='targetCalorieTotalCount'>34.设定运动消耗卡路里目标</a>

	post /rest/v1/targetCalorieTotalCount
	一天一条运动日记，多条运动项目
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
targetCalorieTotalCount | int | 目标消耗卡路里 | NO
createDate | long | 添加时间(11/13位) | NO

####Request Example
	{
		"accessToken":"2313",
		"targetCalorieTotalCount" : 200,
		"createDate": 1417363200
	}

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieTotalCount | int | 实际消耗卡路里 | NO
targetCalorieTotalCount|int | 目标消耗卡路里 | NO
createDate | long | 添加时间(11/13位) | NO
exerices | array | 集合 | NO

##### exerice
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieCount | int | 消耗卡路里 | NO
min | int | 运动时间(分钟) | NO
type | int | 运动类型 | NO
typeName | String | 运动类型名字 | NO

####Response Example Body
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":300,
					"targetCalorieTotalCount":300,
					"totalMinute":38,
					"targetTotalMinute":30,
					"createDate":1417363200,
					"exerices":[
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 120,
							"min": 19,
							"type":1,
							"typeName":"跑步"
						},
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 180,
							"min": 19,
							"type":2,
							"typeName":"游泳"
						}
					]
				}
	
- - - -

###[[⬆]](#toc) <a name='exerciselog_add'>35.添加运动日记</a>
	post /rest/v1/exerciselog
	一天一条运动日记，多条运动项目
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
createDate | long | 添加时间(11/13位) | NO
min | int | 运动时间 | NO
weight | double | 用户体重(kg) | NO
type | int | 运动类型 | NO

####Request Example
	{
		"accessToken":"2313",
		"createDate":1417363200,
		"min": 20,
		"weight": 65.1,
		"type":1
	}

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieTotalCount | int | 实际消耗卡路里 | NO
targetCalorieTotalCount|int | 目标消耗卡路里 | NO
createDate | long | 添加时间(11/13位) | NO
exerices | array | 集合 | NO

##### exerice
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieCount | int | 消耗卡路里 | NO
min | int | 运动时间(分钟) | NO
type | int | 运动类型 | NO
typeName | String | 运动类型名字 | NO

####Response Example Body
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":300,
					"targetCalorieTotalCount":300,
					"totalMinute":38,
					"targetTotalMinute":30,
					"createDate":1417363200,
					"exerices":[
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 120,
							"min": 19,
							"type":1,
							"typeName":"跑步"
						},
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 180,
							"min": 19,
							"type":2,
							"typeName":"游泳"
						}
					]
				}
	
- - - -

###[[⬆]](#toc) <a name='sporteventlist'>36.获取运动项目列表</a>

	get /rest/v1/sporteventlist
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO

####Request Example
	?accessToken=12345643jlkhiaki

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
sportevents | array | 集合 | NO

##### sportevent
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
type | int | 运动类型 | NO
typeName | String | 运动类型名字 | NO
sportImage | String | 运动图片 | NO
unit | double | 单位 | NO
intensity | int | 1:低强度 2:高强度 | NO


####Response Example Body
		[{"uid": "5498db36fd2c6ab600eba367", "typeName": "\u96c6\u4f53\u821e", "intensity": 1, "type": 1, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_running@2x.png"}, {"uid": "5498dc42fd2c6ab600eba368", "typeName": "\u5bb6\u52a1", "intensity": 1, "type": 2, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_bike@2x.png"}, {"uid": "5498dc5ffd2c6ab600eba369", "typeName": "\u722c\u697c\u68af", "intensity": 2, "type": 3, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_skating@2x.png"}, {"uid": "5498dc90fd2c6ab600eba36a", "typeName": "\u75be\u8d70", "intensity": 1, "type": 4, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_swimming@2x.png"}, {"uid": "5498dcb0fd2c6ab600eba36b", "typeName": "\u9a91\u81ea\u884c\u8f66", "intensity": 2, "type": 5, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_basketball@2x.png"}, {"uid": "5498dcd0fd2c6ab600eba36c", "typeName": "\u6253\u62f3/\u7ec3\u529f", "intensity": 1, "type": 6, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_football@2x.png"}, {"uid": "5498dcf2fd2c6ab600eba36d", "typeName": "\u793e\u533a\u5065\u8eab\u5668\u6750\u6d3b\u52a8", "intensity": 1, "type": 7, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_hiking@2x.png"}, {"uid": "5498dd18fd2c6ab600eba36e", "typeName": "\u5176\u4ed6\u9ad8\u5f3a\u5ea6\u6d3b\u52a8", "intensity": 2, "type": 8, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_other1@2x.png"}, {"uid": "5498dd2cfd2c6ab600eba36f", "typeName": "\u5176\u4ed6\u4f4e\u5f3a\u5ea6\u6d3b\u52a8", "intensity": 1, "type": 9, "unit": 1, "sportImage": "http://jenkins.qiniudn.com/img_other2@2x.png"}]
			
- - - -

###[[⬆]](#toc) <a name='surveyresult'>37.提交问卷调查</a>
	post /rest/v1/surveyresult
	可以匿名参加调查
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| YES
uid | String | 评估问卷id | survey==null?NO:YES
survey | String |评估问卷名字 | uid=null?NO:YES
surveyResult | array | 问卷结果 | NO

surveyResult
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | 评估问卷题目id | NO
choice | String | type(0):选择index,type(其他):具体选中的内容 | NO

####Request Example
	{
		"uid":"54a3bca2b2c67d4ab03ee148",
		"surveyResult":[
			{
				"uid":"54a3bca2b2c67d4ab03ee146",
				"choice":"0"
			},
			{
				"uid":"54a3bca2b2c67d4ab03ee147",
				"choice":"1"
			},
			{
				"uid":"54a3bca2b2c67d4ab03ee147",
				"choice":"2015-10-10"
			}
			]
	}

####Response Code
	Status: 200 OK
	Status: 404 问卷或者题目id不存在
	Status: 500 请求参数异常/服务器异常
	
- - - -
###[[⬆]](#toc) <a name='medicineManagement'>38.提交服药情况</a>
	post /rest/v1/medicineManagement
	用于 日历编辑中管理或者设置 剩余药品
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
createDate | long | 服药时间(需要精确到时分) | NO
done | int | 是否服药 1:服药 0:未知 | NO
medicines | array |服药集合|YES

####Request Example
	设置服药
	{
		"accessToken":"111",
		"createDate":1417363200,
		"done":1,
	}
	批量设置
	{
		"accessToken":"111",
		"medicines":[
			{
				"createDate":1417363200,
				"done":0,
			},
		]
	}
	
####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常
	
####Response Example Body
	{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"createDate":1417363200,
		"done":1,
		"remainingMedicine":10
	}
	
	
- - - -
###[[⬆]](#toc) <a name='deleteMedicineManagement'>39.删除服药情况</a>
	delete /rest/v1/medicineManagement
	用于 日历编辑中管理
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String | 服药记录id | YES
createDate | long | 服药记录日期 | YES

####Request Example
	根据id删除
	{
		"accessToken":"111",
		"uid":14173632fdf00
	}
	根据日期删除
	{
		"accessToken":"111",
		"createDate":1417363200
	}

####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常
	
- - - -

- - - -
###[[⬆]](#toc) <a name='medicineList'>40.获取服药记录</a>
	get /rest/v1/medicineList
	由近到远排序
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
page | int | 第几页（0开始, -1获取全部数据） | YES
pageSize | int | 每页返回数量 （默认10条）| YES
done | int | 是否获取全部 1:服药 0:未知(即表示全部) 默认是1 | YES

####Request Example
	?accessToken=12345643jlkhiaki&page=0&pageSize=10

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters
按照时间由近及远排序

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 页数| NO
medicines | array | 集合 | NO
total | int | 总数 | NO

##### medicine
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
createDate | long | 添加时间(11/13位) | NO
remainingMedicine | int | 剩余药片 | NO
done | int | 是否服药 | 1:服药 0:未知 | NO


####Response Example Body
	{
		"page":0,
		"medicines":[
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"createDate":1417363200,
					"done":1,
					"remainingMedicine":10
				},
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdgh",
					"createDate":1417363200,
					"done":1,
					"remainingMedicine":10
				}
			],
		"total":3
	}
- - - -
###[[⬆]](#toc) <a name='patientadd'>41.添加病人</a>
	post /rest/v1/patientadd
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | token| NO
inviteCode | int  | 邀请码(6位数字)| NO

####Request Example
	{
		"accessToken":"xewe",
		"inviteCode":111111
	}

####Response Code
	Status: 200 OK
	Status: 404 验证码错误或者不存在
	Status: 500 请求参数异常/服务器异常
	
####Response Example
	{
		"uid":"01234567-89ab-cdef-0123-456789abcdef",
		"createDate":1417417881,
		"bmiIndex":198,
		"takingDays":11,
		"consumeHeatCount":2009,
		"totalMin":2009
	}
- - - -
###[[⬆]](#toc) <a name='logout'>42.注销</a>
	get /rest/v1/logout
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | token| NO

####Request Example
	?accessToken=xxx

####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常
	
- - - -
###[[⬆]](#toc) <a name='targetTotalMinute'>43.设定运动时间目标</a>

	post /rest/v1/targetTotalMinute
	一天一条运动日记，多条运动项目
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
targetTotalMinute | int | 目标消耗卡路里 | NO
createDate | long | 添加时间(11/13位) | NO

####Request Example
	{
		"accessToken":"2313",
		"targetTotalMinute" : 200,
		"createDate": 1417363200
	}

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieTotalCount | int | 实际消耗卡路里 | NO
targetCalorieTotalCount|int | 目标消耗卡路里 | NO
createDate | long | 添加时间(11/13位) | NO
exerices | array | 集合 | NO

##### exerice
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String  | id(8-4-4-4-12)| NO
calorieCount | int | 消耗卡路里 | NO
min | int | 运动时间(分钟) | NO
type | int | 运动类型 | NO
typeName | String | 运动类型名字 | NO

####Response Example Body
				{
					"uid":"01234567-89ab-cdef-0234-456789abcdeh",
					"calorieTotalCount":300,
					"targetCalorieTotalCount":300,
					"totalMinute":38,
					"targetTotalMinute":30,
					"createDate":1417363200,
					"exerices":[
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 120,
							"min": 19,
							"type":1,
							"typeName":"跑步"
						},
						{
							"uid":"567-89ab-cdef-0234-4",
							"calorieCount": 180,
							"min": 19,
							"type":2,
							"typeName":"游泳"
						}
					]
				}
	
- - - -
###[[⬆]](#toc) <a name='unreadarticlecount'>44.获取未读知识文章数量</a>

	get /rest/v1/knowledge/unreadArticleCount
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO

####Request Example
	?accessToken=xxxx

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常

####Response Parameters

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
unreadArticleCount| int | 未读知识文章的数量 | NO

####Response Example Body
				{
					"unreadArticleCount": 10
				}
	
- - - -
###[[⬆]](#toc) <a name='deleteExerice'>45.删除运动记录</a>
	delete /rest/v1/deleteExerice
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
uid | String | id exericeId | YES

####Request Example
	demo1
	{
		"accessToken":"xxxxf313213",
		"uid":"01234567-89ab-cdef-0123-456789abcdef"
	}
	
	demo2
	/rest/v1/deleteExerice?accessToken=xxx&uid=01234567-89ab-cdef-0123-456789abcdef,01234567-89ab-cdef-0123-456789abcdef

####Response Code
	Status: 200 OK
	Status: 404 数据不存在
	Status: 500 请求参数异常/服务器异常
	
- - - -

###[[⬆]](#toc) <a name='medicineCount'>46.提交剩余药片数量</a>
	post /rest/v1/medicineCount
	用于 设置 剩余药品
	
####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String  | 用户token| NO
remainingMedicine| int | 剩余药片数量| NO

####Request Example
	{
		"accessToken":"111",
		"remainingMedicine":2
	}
	
####Response Code
	Status: 200 OK
	Status: 500 请求参数异常/服务器异常
	
####Response Example Body
	{
		"remainingMedicine":2
	}
	
	
- - - -
###[[⬆]](#toc) <a name='glu_add'>47.添加血糖记录</a>
	post /rest/v1/glu
	用于添加血糖记录

####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String | 用户token | NO
gluIndex0 | double | 血糖记录 | YES
gluIndex1 | double | 血糖记录 | YES
gluIndex2 | double | 血糖记录 | YES
gluIndex3 | double | 血糖记录 | YES
gluIndex4 | double | 血糖记录 | YES
createDate | long | 创建日期 | YES
glus | array | 血糖集合 | YES

####Request Example
	demo1
	{
		"accessToken":"111",
		"gluIndex0":10.33,
		"gluIndex1":10.33,
		"gluIndex2":10.33,
		"gluIndex3":10.33,
		"gluIndex4":10.33,
	}
	demo2
	{
		"accessToken":"111",
		"glus":[{
			"gluIndex0":10.33,
			"gluIndex1":10.33,
			"gluIndex2":10.33,
			"gluIndex3":10.33,
			"gluIndex4":10.33,
			createDate:160103
		}]
	}

####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常
	
####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | id(8-4-4-4-12) | NO
gluIndex0 | double | 血糖记录 | NO
gluIndex1 | double | 血糖记录 | NO
gluIndex2 | double | 血糖记录 | NO
gluIndex3 | double | 血糖记录 | NO
gluIndex4 | double | 血糖记录 | NO
createDate | long | 添加时间(11/13位) | NO

####Response Example Body
	demo1
	{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"gluIndex0":10.33,
		"gluIndex1":10.33,
		"gluIndex2":10.33,
		"gluIndex3":10.33,
		"gluIndex4":10.33,
		"createDate":12345678912
	}
	demo2
	{
		[{
		"uid":"01234567-89ab-cdef-0234-456789abcdeh",
		"gluIndex0":10.33,
		"gluIndex1":10.33,
		"gluIndex2":10.33,
		"gluIndex3":10.33,
		"gluIndex4":10.33,
		"createDate":12345678912
		}]
	}

_ _ _ _

###[[⬆]](#toc) <a name='glu_delete'>48.删除血糖记录</a>
	delete /rest/v1/glu
	删除血糖记录

####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String | 用户token | NO |
uid | String | 血糖记录id(8-4-4-4-12. 为空时, 删除所有记录) | YES |
uids | array | 血糖记录id数列 | YES |

####Request Example
	demo1
	{
		"accessToken":"12345",
		"uid":"01234567-89ab-cdef-0234-456789abcdeh"
	}
	demo2
	{
		"accessToken":"12345"
	}
	demo3
	{
		"accessToken":"12345",
		uids:[
			"01234567-89ab-cdef-0234-456789abcdeh",
			"01234567-89ab-cdef-0234-456789abcbsh"
		]
	}
####Response Code

	Status: 200 OK
	Status: 404 数据不存在
	Status: 500 请求参数异常/服务器异常

_ _ _ _

###[[⬆]](#toc) <a name='glu_list'>49.获取血糖记录</a>
	get /rest/v1/glulist
	获取血糖记录

####Request Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
accessToken | String | 用户token | NO |
profileId | String | 用户id | YES |
page | int | 第几页（0开始, -1获取全部数据）| YES |
pageSize | int | 每页返回数量，默认为10 | YES |

####Request Example

	demo1查看自己
	
	?accessToken=12345643jlkhiaki&page=0&pageSize=10
	
	demo2查看他人
	
	?accessToken=12345643jlkhiaki&page=0&pageSize=10&profileId=12345
	
####Response Code
	Status: 200 OK
	Status: 401 token过期
	Status: 500 请求参数异常/服务器异常
	
####Response Parameters
名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
page | int | 页数 | NO |
glus | array | 血糖集合 | NO |
total | int | 血糖总数 | NO |

#####glu

名字 | 类型 | 描述 | 是否可选
------------ | ------------- | ------------| :-----------:
uid | String | 血糖记录id(8-4-4-4-12) | NO |
gluIndex0 | double  |血糖记录 | NO |
gluIndex1 | double  |血糖记录 | NO |
gluIndex2 | double  |血糖记录 | NO |
gluIndex3 | double  |血糖记录 | NO |
gluIndex4 | double  |血糖记录 | NO |
createDate | long | 创建日期(11/13位) | NO |

####Response Example Body
	{
		"page":0
		"glus":[
			{
				"uid":"01234567-89ab-cdef-0234-456789abcdeh",
				"gluIndex0":10.33,
				"gluIndex1":10.33,
				"gluIndex2":10.33,
				"gluIndex3":10.33,
				"gluIndex4":10.33,
				"createDate":43562718122
			},
			{
				"uid":"01234567-89ab-cdef-0234-456789ertdeh",
				"gluIndex0":10.33,
				"gluIndex1":10.33,
				"gluIndex2":10.33,
				"gluIndex3":10.33,
				"gluIndex4":10.33,
				"createDate":43562718113
			}
		],
		"total":2
	}
_ _ _ _