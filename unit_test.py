# -*- coding: utf-8 -*-
__author__ = 'liubei'

import unittest
import rest
import datetime
import tempfile
import shutil
import requests
import topic
import db_engine
import db_dashboard
import time
import pytz
import az_dateutil
import surveyInstanceDB
import statistics_export

import test.test_support
db_engine = test.test_support.import_module('db_engine')
threading = test.test_support.import_module('threading')

hostUrl = "http://0.0.0.0:4001"
# hostUrl = "http://216.89.223.132:8000"

class BaseTestCase(unittest.TestCase):
    def setUp(self):
        self.work_path = tempfile.mkdtemp()
        self.work_path2 = tempfile.mkdtemp()
        # 这里也是对线程的特殊处理
        self._threads = test.test_support.threading_setup()

    def tearDown(self):
        shutil.rmtree(self.work_path)
        shutil.rmtree(self.work_path2)
        # 这里也是对线程的特殊处理
        test.test_support.threading_cleanup(*self._threads)
        test.test_support.reap_children()


class TestMethods(BaseTestCase):
    def test_thisWeekFirstDate(self):
        #3.23.2015
        day = 1427040000
        result = [datetime.datetime(2015, 3, 23, 0, 0), datetime.datetime(2015, 3, 24, 0, 0), datetime.datetime(2015, 3, 25, 0, 0), datetime.datetime(2015, 3, 26, 0, 0), datetime.datetime(2015, 3, 27, 0, 0), datetime.datetime(2015, 3, 28, 0, 0), datetime.datetime(2015, 3, 29, 0, 0)]
        print rest.thisWeek(datetime.datetime.fromtimestamp(day))
        self.assertEqual(result, rest.thisWeek(datetime.datetime.fromtimestamp(day)))
        #3.24.2015
        print rest.thisWeek(datetime.datetime.fromtimestamp(day + 24*60*60))
        self.assertEqual(result, rest.thisWeek(datetime.datetime.fromtimestamp(day + 24*60*60)))
        #3.25.2015
        print rest.thisWeek(datetime.datetime.fromtimestamp(day + 2*24*60*60))
        self.assertEqual(result, rest.thisWeek(datetime.datetime.fromtimestamp(day + 2*24*60*60)))
        #3.26.2015
        print rest.thisWeek(datetime.datetime.fromtimestamp(day + 3*24*60*60))
        self.assertEqual(result, rest.thisWeek(datetime.datetime.fromtimestamp(day + 3*24*60*60)))
        #3.27.2015
        print rest.thisWeek(datetime.datetime.fromtimestamp(day + 4*24*60*60))
        self.assertEqual(result, rest.thisWeek(datetime.datetime.fromtimestamp(day + 4*24*60*60)))
        #3.28.2015
        print rest.thisWeek(datetime.datetime.fromtimestamp(day + 5*24*60*60))
        self.assertEqual(result, rest.thisWeek(datetime.datetime.fromtimestamp(day + 5*24*60*60)))
        #3.29.2015
        print rest.thisWeek(datetime.datetime.fromtimestamp(day + 6*24*60*60))
        self.assertEqual(result, rest.thisWeek(datetime.datetime.fromtimestamp(day + 6*24*60*60)))

    def test_createMedicineFix(self):
        print db_engine.createMedicineFix({"userPhone": "18018626781", "done": 1, "createDate": 1432314000})
        print db_engine.createMedicineFix({"userPhone": "18018626781", "done": 0, "createDate": 1432314000})
        print db_engine.createMedicineFix({"userPhone": "18018626781", "done": 1, "createDate": 1432224000})
        print db_engine.createMedicineFix({"userPhone": "18018626781", "done": 0, "createDate": 1432224000})

    def test_logLongMessage(self):
        print requests.get(hostUrl + "/rest/v1/knowledge/article/54b73fc4b2c67d3f6d51925b?accessToken=luDHYRiOWZIzfvAp").json()
        print requests.get(hostUrl + "/rest/v1/knowledge/article/54b4ec53b2c67d022495a7eb?accessToken=luDHYRiOWZIzfvAp").json()
        print requests.get(hostUrl + "/rest/v1/knowledge/article/54b73fc4b2c67d3f6d51925b?accessToken=AMyrOcDlCkfKdtez").json()
        print requests.get(hostUrl + "/rest/v1/knowledge/article/54b4ec53b2c67d022495a7eb?accessToken=AMyrOcDlCkfKdtez").json()

    def test_logLongMessageCatalog(self):
        headers = {'user-agent': 'my-app/0.0.1'}
        print requests.get(hostUrl + "/rest/v1/knowledge/topic/54b4ec53b2c67d022495a7e9?accessToken=luDHYRiOWZIzfvAp", headers=headers).json()
        print requests.get(hostUrl + "/rest/v1/knowledge/topic/54b4ec53b2c67d022495a7f4?accessToken=luDHYRiOWZIzfvAp", headers=headers).json()
        print requests.get(hostUrl + "/rest/v1/knowledge/topic/54b4ec53b2c67d022495a7f4?accessToken=AMyrOcDlCkfKdtez", headers=headers).json()
        print requests.get(hostUrl + "/rest/v1/knowledge/topic/54b4ec53b2c67d022495a7e9?accessToken=AMyrOcDlCkfKdtez", headers=headers).json()

    def test_addTargetMin(self):
        postJson = "{'accessToken': 'kLVTbSdhCXGEFgae', 'createDate': 1434902400, 'targetTotalMinute': 145}"
        url = hostUrl + "/rest/v1/targetTotalMinute"
        print requests.post(url, data=postJson)

    def test_addExerice(self):
        postJson = "{'accessToken': 'kLVTbSdhCXGEFgae', 'createDate': 1434902400, 'type': 1, 'min':2, 'weight':0}"
        url = hostUrl + "/rest/v1/exerciselog"
        print requests.post(url, data=postJson).json()

    def test_QuestionRecord(self):
        db_engine.connect("az", host=db_engine.host)
        topic.QuestionRecord.objects()

    def test_lastHome(self):
        now = int(time.time())
        tz = pytz.timezone(pytz.country_timezones('cn')[0])
        thisDate = datetime.datetime.fromtimestamp(now, tz)
        now = int(az_dateutil.date2timestamp(
                thisDate.replace(thisDate.year, thisDate.month, thisDate.day, 0, 0, 0))) + 16 * 60 * 60
        db_dashboard.queryHomeLogBetweenDays(now - 7*24*60*60, now, "18001822249")

    def test_survey(self):
        surveyInstanceDB.checkSurveySchedule()

    def test_queryUserBetweenDay(self):
        users = db_dashboard.queryUserBetweenDays(1410624000, 1410670440)
        print "total: ", users.count()
        for user in users:
            print "[userPhone: %s], [E-Code: %s], [Equestion: %s], [CreateDate: %s]" \
                  % (user.userPhone, user.patientCode, user.userType==2, az_dateutil.timestamp2datetime(user.createDate).strftime("%d %b %Y"))

    def test_ne(self):
        db_engine.connect('az', host=db_engine.host)
        users = db_engine.User.objects(userType__ne=2).order_by('-createDate')
        print users.count()
        for user in users:
            print user.patientCode

    def testDiffDay(self):
        print statistics_export.diffDay(az_dateutil.timestamp2datetime(1443166644))
        print statistics_export.diffDay(az_dateutil.timestamp2datetime(1443502800))
        print statistics_export.diffDay(az_dateutil.timestamp2datetime(1443506400))

if __name__ == '__main__':
    # unittest.main()
    loader = unittest.TestLoader()
    # loader.testMethodPrefix = 'test_notify_any_change'
    suite = loader.loadTestsFromTestCase(TestMethods)
    unittest.TextTestRunner(verbosity=3).run(suite)