# api
## python 依赖库
- sudo apt-get install python-setuptools python-dev build-essential
- sudo easy_install web.py
- sudo easy_install mongoengine
- sudo easy_install schedule
- sudo easy_install simplejson
- sudo easy_install pytz
- sudo easy_install pycrypto
- sudo pip install mongolog
- pip install pymongo==2.8（[如果显示连接数据库失败，可以尝试安装](https://github.com/MongoEngine/mongoengine/issues/935)）


## 数据库
For 64-bit yum源配置：

	vi /etc/yum.repos.d/10gen.repo
 ~

	[10gen]
	name=10gen Repository
	baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64
	gpgcheck=0
	
~

	yum install mongo-10gen-server
	
~

	sudo /etc/init.d/mongod start
	
ubuntu

	http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/
	
## 其他
	sudo yum install git
	

## 安装
	
	git clone https://git.oschina.net/beiliubei/api_define.git
	

## 一些脚本
	kill -9 `ps aux|grep 'python2.7 rest.py 0.0.0.0:8000' |grep -v grep |grep 'python2.7 rest.py 0.0.0.0:8000' |awk '{print $2}'`
	kill -9 `ps aux|grep 'python2.7 surveyInstanceDB.py' |grep -v grep |grep 'python2.7 surveyInstanceDB.py' |awk '{print $2}'`
	cd /root/home/bmp/download/api_define_test/
	nohup python2.7 rest.py 0.0.0.0:8000 > myout.file 2>&1 &
	nohup python2.7 surveyInstanceDB.py > nohup.log 2>&1 &


## 云片短信

    python sms.py

    查看服务器ip是否加入到白名单中

# 数据导出
## 依赖库
- sudo pip install box.py
- sudo pip install openpyxl

# 日志
## 使用mongolog
	 use mongolog
	 db.createCollection('log', {capped:true, size:100000})