# -*- coding: utf-8 -*-
__author__ = 'liubei'

import topic
import db_engine
import csv

if __name__ == "__main__":
    db_engine.connect('az', host=db_engine.host)
    with open('article.csv', 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for row in spamreader:
            articles = topic.Article.objects(articleIndex=row[0])
            if articles:
                article = articles[0]
                content = topic.articleHtml % (article.title, '', '', article.image, article.des, row[1].decode("utf-8"))
                print content
                article.update(set__content=content)