# -*- coding: utf-8 -*-
__author__ = 'liubei'

from mongoengine import *
import time
import db_engine
import api_statistics

host = db_engine.host

class Exerice(DynamicDocument):
    calorieCount = FloatField(required=True)
    typeName = StringField(required=True)
    min = IntField(required=True)
    exericeId = StringField(required=True)
    type = IntField(required=True)


class Exericelog(DynamicDocument):
    userPhone = StringField(required=True)
    targetCalorieTotalCount = FloatField(required=True)
    createDate = IntField(required=True)
    calorieTotalCount = FloatField(required=True)
    totalMinute = IntField(required=True)
    targetTotalMinute = IntField(required=True)


class Bmi(DynamicDocument):
    targetBmiIndex = FloatField(required=True)
    bmiIndex = FloatField(required=True)
    weight = FloatField(required=True)
    targetWeight = FloatField(required=True)
    createDate = IntField(required=True)
    height = FloatField(required=True)
    userPhone = StringField(required=True)


class Smoke(DynamicDocument):
    count = IntField(required=True)
    createDate = IntField(required=True)
    userPhone = StringField(required=True)


class Bp(DynamicDocument):
    bpHighIndex = IntField(required=True)
    bpLowIndex = IntField(required=True)
    createDate = IntField(required=True)
    userPhone = StringField(required=True)


class Cholesterol(DynamicDocument):
    high = StringField(required=True)
    low = StringField(required=True)
    tg = StringField(required=True)
    tc = StringField(required=True)
    createDate = IntField(required=True)
    userPhone = StringField(required=True)
    createDateString = StringField(required=True)


def queryExericeLogBetweenDays(startDay, endDay, userPhone):
    connect('az', host=host)
    return Exericelog.objects(userPhone=userPhone, createDate__lte=startDay, createDate__gte=endDay).order_by('-createDate')

def queryExericeLogByUserPhone(userPhone):
    connect('az', host=host)
    return Exericelog.objects(userPhone=userPhone).order_by('-createDate')

def queryExerices(logId):
    connect('az', host=host)
    return Exerice.objects(exericeId=logId)

def queryExericeByLogId(logId):
    connect('az', host=host)
    return Exerice.objects(exericeId=logId)


def queryBmiBetweenDays(startDay, endDay, userPhone):
    connect('az', host=host)
    return Bmi.objects(userPhone=userPhone, createDate__lte=startDay, createDate__gte=endDay).order_by('-createDate')


def queryBpBetweenDays(startDay, endDay, userPhone):
    connect('az', host=host)
    return Bp.objects(userPhone=userPhone, createDate__lte=startDay, createDate__gte=endDay).order_by('-createDate')


def querySmokeBetweenDays(startDay, endDay, userPhone):
    connect('az', host=host)
    return Smoke.objects(userPhone=userPhone, createDate__lte=startDay, createDate__gte=endDay).order_by('-createDate')


def queryMedicinelistBetweenDays(startDay, endDay, userPhone):
    connect('az', host=host)
    return db_engine.Medicine.objects(userPhone=userPhone, createDate__lt=startDay, createDate__gte=endDay).order_by('-createDate')


def queryCholesterolBetweenDays(startDay, endDay, userPhone):
    connect('az', host=host)
    return Cholesterol.objects(userPhone=userPhone, createDate__lte=startDay, createDate__gte=endDay).order_by('-createDate')


def queryHomeLogBetweenDays(startDay, endDay, userPhone):
    connect('az', host=host)
    return api_statistics.HomePopularity.objects(userPhone=userPhone, lastModifyDate__lte=endDay, lastModifyDate__gte=startDay).order_by('-lastModifyDate')


def queryUserBetweenDays(startDay, endDay):
    connect('az', host=host)
    return db_engine.User.objects(createDate__lte=endDay, createDate__gte=startDay).order_by('-createDate')


if __name__ == "__main__":
    now = int(time.time())
    print now
    print now - 4*7*24*60*60
    # print queryExericeLogBetweenDays(now, now - 7*24*60*60, "18001822249")
    print queryBmiBetweenDays(now, now - 4*7*24*60*60, "18001822249")
    # print queryBpBetweenDays(now, now - 7*24*60*60, "18001822249")
    # print querySmokeBetweenDays(now, now - 7*24*60*60, "18001822249")
    # print queryMedicinelistBetweenDays(now, now - 7*24*60*60, "18001822249")
    # print queryCholesterolBetweenDays(now, now - 7*24*60*60, "18001822249")