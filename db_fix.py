# -*- coding: utf-8 -*-
__author__ = 'liubei'

import db_engine
from mongoengine import *
import aes
import time
import db_dashboard

def fix_medicine(delete=False):
    connect('az', host=db_engine.host)
    aes_cipher = aes.AESCipher(aes.aes_key)
    users = db_engine.User.objects()
    for user in users:
        print "====== [ " + user.userPhone + "] ======"
        medicines = db_engine.Medicine.objects(userPhone=user.userPhone)
        x = set()
        for medicine in medicines:
            print "createDate: " + str(medicine.createDate) + ", done: " + str(aes_cipher.decrypt(str(medicine.done)))
            createDay = time.strftime('%Y-%m-%d', time.localtime(medicine.createDate))
            if createDay in x:
                print "xxxxxx======xxxxxx [ " + user.userPhone +" has createDate: " + str(medicine.createDate)\
                      + "] xxxxxx======xxxxxx"
                if delete:
                    db_engine.Medicine.delete(medicine)
                    print "xxxxxx======xxxxxx [ " + user.userPhone +" has delete: " + str(medicine.createDate)\
                      + "] xxxxxx======xxxxxx"
            else:
                x.add(createDay)
        if len(x) > 0:
            print "data: " + str(x)


def fix_exericelog(delete=False):
    connect('az', host=db_engine.host)
    aes_cipher = aes.AESCipher(aes.aes_key)
    users = db_engine.User.objects()
    for user in users:
        print "====== [ " + user.userPhone + "] ======"
        exericelogs = db_dashboard.Exericelog.objects(userPhone=user.userPhone)
        x = set()
        for exericelog in exericelogs:
            print "createDate: " + str(exericelog.createDate) + ", totalMin: " + str(aes_cipher.decrypt(str(exericelog.totalMinute)))
            createDay = time.strftime('%Y-%m-%d', time.localtime(exericelog.createDate))
            if createDay in x:
                print "xxxxxx======xxxxxx [ " + user.userPhone +" has createDate: " + str(exericelog.createDate)\
                      + "] xxxxxx======xxxxxx"
                if delete:
                    db_dashboard.Exericelog.delete(exericelog)
                    print "xxxxxx======xxxxxx [ " + user.userPhone +" has delete: " + str(exericelog.createDate)\
                      + "] xxxxxx======xxxxxx"
            else:
                x.add(createDay)
        if len(x) > 0:
            print "data: " + str(x)


def fix_cholesterol(delete=False):
    connect('az', host=db_engine.host)
    aes_cipher = aes.AESCipher(aes.aes_key)
    users = db_engine.User.objects()
    for user in users:
        print "====== [ " + user.userPhone + "] ======"
        cholesterols = db_dashboard.Cholesterol.objects(userPhone=user.userPhone)
        x = set()
        for cholesterol in cholesterols:
            print "createDate: " + str(cholesterol.createDate) + ", tc: " + str(aes_cipher.decrypt(str(cholesterol.tc)))
            createDay = time.strftime('%Y-%m-%d', time.localtime(cholesterol.createDate))
            if createDay in x:
                print "xxxxxx======xxxxxx [ " + user.userPhone +" has createDate: " + str(cholesterol.createDate)\
                      + "] xxxxxx======xxxxxx"
                if delete:
                    db_dashboard.Cholesterol.delete(cholesterol)
                    print "xxxxxx======xxxxxx [ " + user.userPhone +" has delete: " + str(cholesterol.createDate)\
                      + "] xxxxxx======xxxxxx"
            else:
                x.add(createDay)
        if len(x) > 0:
            print "data: " + str(x)


if __name__ == "__main__":
    fix_medicine(True)
    print 'xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx'
    fix_exericelog(True)
    print 'xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx==xx'
    fix_cholesterol(True)