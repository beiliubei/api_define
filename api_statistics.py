__author__ = 'liubei'


from mongoengine import *
import time
import db_engine
import topic
from bson.objectid import ObjectId

host = db_engine.host

class LongMessagePopularity(DynamicDocument):
    totalCount = IntField(required=True)
    msgNo = StringField(required=True)
    users = ListField(ReferenceField(db_engine.User))
    lastModifyDate = LongField(required=True)

class LongMessageCatalogPopularity(DynamicDocument):
    totalCount = IntField(required=True)
    catalogNo = StringField(required=True)
    users = ListField(ReferenceField(db_engine.User))
    lastModifyDate = LongField(required=True)


class QuestionPopularity(DynamicDocument):
    totalCount = IntField(required=True)
    lastModifyDate = LongField(required=True)
    userPhone = StringField(required=True)


class HomePopularity(DynamicDocument):
    lastModifyDate = LongField(required=True)
    userPhone = StringField(required=True)


def logLongMessage(userPhone, msgNo):
    db_engine.connect('az', host=host)
    longMessagePopularities = LongMessagePopularity.objects(msgNo=msgNo)
    user = db_engine.User.objects(userPhone=userPhone)[0]
    if len(longMessagePopularities) == 0:
        LongMessagePopularity(totalCount=1,
                              lastModifyDate=int(time.time()),
                              users=[user],
                              msgNo=msgNo).save()
    else:
        longMessagePopularity = longMessagePopularities[0]
        contain = False
        for u in longMessagePopularity.users:
            if u.id == user.id:
                contain = True
                break
        if not contain:
            longMessagePopularity.users.append(user)
        longMessagePopularity.totalCount += 1
        longMessagePopularity.update(set__lastModifyDate=int(time.time()),
                                     set__totalCount=longMessagePopularity.totalCount,
                                     set__users=longMessagePopularity.users)


def logLongMessageCatalog(userPhone, uid):
    db_engine.connect('az', host=host)
    catalogNo = topic.Topic.objects(_id=ObjectId(uid))[0].index
    longMessagePopularities = LongMessageCatalogPopularity.objects(catalogNo=catalogNo)
    user = db_engine.User.objects(userPhone=userPhone)[0]
    if len(longMessagePopularities) == 0:
        LongMessageCatalogPopularity(totalCount=1,
                                     lastModifyDate=int(time.time()),
                                     users=[user],
                                     catalogNo=catalogNo).save()
    else:
        longMessagePopularity = longMessagePopularities[0]
        contain = False
        for u in longMessagePopularity.users:
            if u.id == user.id:
                contain = True
                break
        if not contain:
            longMessagePopularity.users.append(user)
        longMessagePopularity.totalCount += 1
        longMessagePopularity.update(set__lastModifyDate=int(time.time()),
                                     set__totalCount=longMessagePopularity.totalCount,
                                     set__users=longMessagePopularity.users)


def logQuestion(userPhone):
    db_engine.connect('az', host=host)
    longMessagePopularities = QuestionPopularity.objects(userPhone=userPhone)
    if len(longMessagePopularities) == 0:
        QuestionPopularity(totalCount=1,
                              lastModifyDate=int(time.time()),
                              userPhone=userPhone).save()
    else:
        longMessagePopularity = longMessagePopularities[0]
        longMessagePopularity.totalCount += 1
        longMessagePopularity.update(set__lastModifyDate=int(time.time()),
                                     set__totalCount=longMessagePopularity.totalCount)


def logHome(userPhone):
    db_engine.connect('az', host=host)
    HomePopularity(lastModifyDate=int(time.time()),
                              userPhone=userPhone).save()